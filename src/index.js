import React from 'react'
import ReactDOM from 'react-dom/client'
import AppContext from './context/AppContext'
import App from './App'
import './minty.css.css'
import './css/style.css'

const container = document.getElementById('root')
const root = ReactDOM.createRoot(container)
root.render(
  <React.StrictMode>
    <AppContext>
      <App />
    </AppContext>
  </React.StrictMode>
)
