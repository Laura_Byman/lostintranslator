import React from 'react'

const ProfileMessageHistory = ({ translations }) => {
  const translationsList = translations.map((translation, index) => (
    <li key={index + ' - ' + translation}>{translation}</li>
  ))
  return (
    <div>
      {translationsList.length !== 0 && <h4>Your 10 last translations:</h4>}
      {translationsList}
    </div>
  )
}

export default ProfileMessageHistory
