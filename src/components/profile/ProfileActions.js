import React from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { Button, Col, Row } from 'react-bootstrap'
import { useUser } from '../../context/UserContext'
import { STORAGE_KEY_USER } from '../../const/storageKeys'
import { storageRemove, storageSave } from './../utils/storage'
import { translationClearHistory } from '../api/translationsAPI'

const ProfileActions = () => {
  const { user, setUser } = useUser()
  const navigate = useNavigate()

  const handleClearHistory = async () => {
    if (
      !window.confirm('Are you sure you want to clear all translation history!')
    ) {
      return
    }

    const [clearError] = await translationClearHistory(user.id)

    if (clearError !== null) {
      return
    }

    const updateUser = {
      ...user,
      translations: []
    }

    storageSave(STORAGE_KEY_USER, updateUser)
    setUser(updateUser)
  }

  return (
    <Col>
      <Col className="pt-4 pb-4">
        <Link to="/translator">Sign Translator</Link>
      </Col>

      {user.translations.length > 0 && (
        <Col className="pt-4 pb-4 ">
          <Button onClick={handleClearHistory}>Clear History</Button>
        </Col>
      )}
    </Col>
  )
}

export default ProfileActions
