import React from 'react'

const ProfileHeader = ({ username }) => {
  return <h4>Welcome back {username}</h4>
}

export default ProfileHeader
