import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { Navbar, Nav } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'
import { useUser } from '../../context/UserContext'
import AccountCircleIcon from '@mui/icons-material/AccountCircle'
import SubtitlesIcon from '@mui/icons-material/Subtitles'
import LogoutIcon from '@mui/icons-material/Logout'
import SignLanguageIcon from '@mui/icons-material/SignLanguage'
import { storageRemove } from './../utils/storage'
import { STORAGE_KEY_USER } from '../../const/storageKeys'

const Header = () => {
  const { user, setUser } = useUser()

  const navigate = useNavigate()

  const handleLogout = () => {
    if (window.confirm('Are you sure you want to log out?')) {
      storageRemove(STORAGE_KEY_USER)
      setUser(null)
      navigate('/')
    }
  }

  return (
    <Navbar
      fixed="top"
      collapseOnSelect
      expand="lg"
      bg="dark"
      variant="dark"
      className="navbar p-3"
    >
      <Link to="/" className="navbar-brand">
        <SignLanguageIcon /> Lost In Translator
      </Link>
      <Navbar.Toggle
        aria-controls="responsive-navbar-nav"
        className="navbar-toggler d-flex d-lg-none"
      />
      <Navbar.Collapse id="responsive-navbar-nav" className="collapse">
        {/*IF LOGGED SHOW ALL LINKS IN OR MAIN PAGE AND LOGOUT*/}
        {user !== null && (
          <Nav className="links">
            <Link to="/profile" className="nav-link">
              <AccountCircleIcon /> {user.username}
            </Link>
            <Link to="/translator" className="nav-link">
              <SubtitlesIcon /> Translator
            </Link>
            <Link to={`/`} className="nav-link" onClick={handleLogout}>
              <LogoutIcon /> Logout
            </Link>
          </Nav>
        )}
      </Navbar.Collapse>
    </Navbar>
  )
}

export default Header
